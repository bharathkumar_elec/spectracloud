package main

import (
	"errors"
        log "github.com/sirupsen/logrus"
)

type DB interface {
	init()
	execute(string) ([]string, error)
}

func get_db(db_type string) (DB,error){
	if db_type=="sqlite3"{
		db := &sqlite3{}
		return db, nil
	}
	if db_type=="mongo"{
		db := &mongo{}
		return db, nil
	}
	return nil, errors.New("Invalid DB type")
}



type sqlite3 struct{
	prefix string
}

func (s *sqlite3) init() {
	log.Info("Initializaing Sqlite3 DB")
	s.prefix = "Sqlite3-"
}

func (s *sqlite3) execute(query string) ([]string, error) {
	if query == "invalid" {
		return []string{}, errors.New("Invalid Query")
	}
	return []string{s.prefix+query}, nil
}


type mongo struct{
        prefix string
}

func (m *mongo) init() {
        log.Info("Initializaing Sqlite3 DB")
        m.prefix = "Mongo-"
}

func (m *mongo) execute(query string) ([]string, error) {
        if query == "invalid" {
                return []string{}, errors.New("Invalid Query")
        }
        return []string{m.prefix+query}, nil
}

