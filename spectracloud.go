package main

import  (
		"time"
		"errors"
		"os"
		"context"
		"sync"
		"strconv"
		log "github.com/sirupsen/logrus"
	)

var (
	contextKeyTask = contextKey("task")
	taskchan = make(chan task)
)

type contextKey string

func (c contextKey) String() string {
    return "contextkey-" + string(c)
}

type Response interface {
	setResult([]string)
	setError(error)
	setCompleted(bool)
	getError() error
	getResult() []string
	getComm() chan Comm
	setComm(chan Comm)
	waitTillComplete()
	subscribe(func(string, []string), func(string), func(string, error))
	getCmd() string
}

type Comm struct {
	Result []string
	status bool
	err  error
}

type ResponseStruct struct {
	ctx context.Context
	err error
	Result []string
	status bool
	comm chan Comm
	cmd string
}

func (rs *ResponseStruct) getError() error {
	return rs.err
}

func (rs *ResponseStruct) getCmd() string {
	return rs.cmd
}

func (rs *ResponseStruct) getResult() []string {
	return rs.Result
}

func (rs *ResponseStruct) setError(err error) {
	rs.err = err
}

func (rs *ResponseStruct) setResult(data []string) {
	rs.Result = data
}

func (rs *ResponseStruct) setCompleted(status bool) {
	rs.status = status
}

func (rs *ResponseStruct) setComm(comm chan Comm) {
        rs.comm = comm
}

func (rs *ResponseStruct) getComm() (chan Comm) {
        return rs.comm
}

func (rs *ResponseStruct) waitTillComplete() {
	//ctx := rs.ctx
	rs_comm := rs.getComm()
	select {
	/*
	case <- ctx.Done():
		rs.err = ctx.Err()
		rs.status = "TimedOut"
		return
	*/
	case comm:= <-rs_comm :
		rs.setResult(comm.Result)
		rs.setCompleted(comm.status)
		rs.setError(comm.err)
		/*
		rs.status = comm.status
		rs.Result = comm.result
		rs.err = comm.err
		*/
		return
	}
}

func (rs *ResponseStruct) subscribe(result_func func(string, []string), completion_func func(string), error_func func(string, error)) {
	log.Debug("In Subscription")
	go rs.subscribe_routine(result_func, completion_func, error_func)
}

func (rs *ResponseStruct) subscribe_routine(result_func func(string, []string), completion_func func(string), error_func func(string, error)) {
	//ctx := rs.ctx
	rs_comm := rs.getComm()
	cmd := rs.getCmd()
	select {
	/*
	case <- ctx.Done():
		rs.err = ctx.Err()
		rs.status = "TimedOut"
		error_func(rs.err)
		return
	*/
	case comm:= <-rs_comm :
		rs.setResult(comm.Result)
		rs.setCompleted(comm.status)
		rs.setError(comm.err)
		/*
		rs.status = comm.status
		rs.Result = comm.result
		rs.err = comm.err
		if rs.err != nil {
			error_func(rs.err)
		}
		*/
		completion_func(cmd)
		if !comm.status {
			result_func(cmd, comm.Result)
		}
		error_func(cmd, comm.err)
		return
	}
}

type task struct {
	ctx context.Context
	cmd string
	//Func func() (int, error)
	ResponseComm chan Comm
}

type Operation struct {
	task task
	taskchan chan task
	taskchanreadlock sync.Mutex
	taskchanwritelock sync.Mutex
	processchan chan task
	control_enqueue chan string
	control_dequeue chan string
	control_ack chan string
	Func func(DB, string) ([]string, error)
	ctx context.Context
	dbhandle DB
	timeout time.Duration
}

func (o *Operation) init(Func func(DB, string) ([]string, error)) {
	// Start the Workers
	o.taskchan = make(chan task, 100)
	o.processchan = make(chan task, 1)
	o.control_enqueue = make(chan string, 1)
	o.control_dequeue = make(chan string, 1)
	o.control_ack = make(chan string, 1)
	o.Func = Func
	o.timeout = 5
	go o.worker(o.taskchan)
	go o.enqueue()
	go o.cleaner()
}

func (o *Operation) setDB(db DB) {
	o.dbhandle= db
}

func (o *Operation) execute(cmd string ) Response {
	log.Debug("Executing Operation")

	ctx, _ := context.WithTimeout(context.Background(), o.timeout*time.Second)
	Response := &ResponseStruct{comm: make(chan Comm), ctx:ctx, Result:[]string{}, cmd:cmd}
	in_task := task{ResponseComm: Response.comm, ctx:ctx, cmd:cmd}
	o.processchan <- in_task
	return Response
}

func (o *Operation) enqueue() {
	log.Debug("Starting the Enqueue Process")
	for {
		select  {
		case in_task:= <-o.processchan:
			o.taskchan <- in_task
			log.Info("Task Enqueued:", in_task)

		case cmd := <-o.control_enqueue:
			if cmd == "pause" {
				o.control_ack<- "Ack"
				log.Debug("Paused the enqueue ")
				K:
				for {
					select {
					case cmd = <-o.control_enqueue:
						if cmd == "resume" {
							log.Debug("Resumed the enqueue")
							break K
						}
					}
				}
			}
		}
	}
}

func (o *Operation) cleaner() {
	for {
		time.Sleep(20*time.Second)
		log.Debug("Control pause")
		o.control_enqueue<- "pause"
		<-o.control_ack
		o.control_dequeue<- "pause"
		<-o.control_ack
		log.Debug("Total Elements in queue", len(o.taskchan))
		queue_length := len(o.taskchan)
		tmp_taskchan := make(chan task, 500)
		if len(o.taskchan) != 0 {
			for i:=0 ; i<queue_length; i++ {
			//for task := range o.taskchan {
				log.Debug("Element:", i)
				task := <-o.taskchan
				dl,_ := task.ctx.Deadline()
				log.Info("task", task, "--> Deadline:", dl)
				if !time.Now().After(dl) {
					tmp_taskchan<- task
				} else {
					log.Info("Deleting Task", task)
					task.ResponseComm<- Comm{
						status: true,
						err: errors.New("Expired"),
					}
				}
			}
		}
		tmp_queue_length := len(tmp_taskchan)
		if tmp_queue_length  != 0 {
			for i:=0 ; i<tmp_queue_length; i++ {
			//for task := range tmp_taskchan {
				task := <-tmp_taskchan
				log.Info("Adding Element Back to taskchan:", task)
				o.taskchan<- task
			}
		}
		log.Info("Total Elements in queue:", len(o.taskchan))
		log.Debug("Control resume")
		o.control_enqueue<- "resume"
		o.control_dequeue<- "resume"
	}
}

func (o *Operation)worker(taskchan <-chan task) {
	log.Debug("Starting Worker")
	for  {
		log.Debug("Interation")
		select {
                case cmd := <-o.control_dequeue:
                        if cmd == "pause" {
                                log.Debug("Paused the dequeue ")
                                o.control_ack<- "Ack"
                                L:
                                for {
                                        select {
                                        case cmd = <-o.control_dequeue:
                                                if cmd == "resume" {
                                                        log.Debug("Resumed the dequeue")
                                                        break L
                                                }
                                        }
                                }
                        }
		case in_task := <-o.taskchan:
			dl,_ := in_task.ctx.Deadline()
			if time.Now().After(dl) {
			log.Info("Deadline Already Expired: ", in_task.cmd)
				in_task.ResponseComm<- Comm{
					status: true,
					err: errors.New("TimedOut"),
				}
			} else {
				log.Info("Processing the Task", in_task)
				//value, err := o.Func(in_task.cmd)
				c := make(chan Comm, 1)
				go func(){
					value, err := o.Func(o.dbhandle, in_task.cmd)
					c<- Comm{
						Result: value,
						status: true,
						err: err,
				}
				}()
				select {
					case <-in_task.ctx.Done():
						in_task.ResponseComm<-Comm{
							status: true,
							err: errors.New("TimedOut"),
						}
					case comm:=<-c:
						in_task.ResponseComm<- comm
				}
				/*
				value, err := o.Func(o.dbhandle, in_task.cmd)
				in_task.ResponseComm<- Comm{
					Result: value,
					status: true,
					err: err,
				}
				*/
			}

		}
	}
	log.Debug("End------")
}

func db_execute(db DB, cmd string) ([]string, error){
	return db.execute(cmd)
}
func main(){
	log.SetOutput(os.Stdout)
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(&log.TextFormatter{
		DisableColors: true,
		FullTimestamp: true,
	})

	log.Info("Starting spectra cloud!!!!")
	db,err := get_db("sqlite3")
	if err !=nil {
		log.Fatal("Invalid DB type")
	}
	db.init()
	operation := Operation{}
	operation.init(db_execute)
	/*
	operation.init(func(cmd string) ([]string, error){
		time.Sleep(10*time.Second)
		return []string{cmd}, nil
	})
	*/
	operation.setDB(db)
	{
	resp := operation.execute("1111")
	resp.waitTillComplete()
	log.Info(resp)
	}
	{
	resp := operation.execute("2222")
	resp.subscribe(func(cmd string, result []string){log.Info(cmd,":Result is ", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){log.Debug(cmd,": Error:",err)})
	}
	{
	resp := operation.execute("3333")
	resp.subscribe(func(cmd string, result []string){log.Info(cmd,":Result is ", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){log.Debug(cmd,": Error:",err)})
	}
	{
	index:=0
	for  {
	log.Debug("==>",index)
	time.Sleep(5*time.Second)
	resp := operation.execute("cmd-"+strconv.Itoa(index))
	resp.subscribe(func(cmd string, result []string){log.Info(cmd,":Result is ", result)}, func(cmd string){log.Info(cmd,":Task is Completed:")}, func(cmd string, err error){log.Info(cmd,": Error:",err)})
	index = index+1
	}
	}

	for {}
}


