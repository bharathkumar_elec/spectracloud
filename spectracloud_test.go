package main


import (
	//"errors"
	"time"
	"testing"
	log "github.com/sirupsen/logrus"
	. "gopkg.in/check.v1"
)
func Test(t *testing.T) { TestingT(t) }

type MySuite struct{}

var _ = Suite(&MySuite{})

func (s *MySuite) SetUpTest(c *C) {
}

func (s *MySuite) TestDB(c *C) {
  {
	log.Info("Testing sqlite3")
	db, err := get_db("sqlite3")
	c.Assert(db, Not(Equals), nil)
	c.Assert(err, IsNil)
	db.init()
	{
	value, err := db.execute("test")
	c.Assert(value[0], Equals, "Sqlite3-test")
	c.Assert(err, IsNil)
	}
	{
	_, err := db.execute("invalid")
	c.Assert(err, NotNil)
	}
  }
  {
        log.Info("Testing Mongo")
        db, err := get_db("mongo")
        c.Assert(db, Not(Equals), nil)
        c.Assert(err, IsNil)
        db.init()
        {
        value, err := db.execute("test")
        c.Assert(value[0], Equals, "Mongo-test")
        c.Assert(err, IsNil)
        }
        {
        _, err := db.execute("invalid")
        c.Assert(err, NotNil)
        }
  }
}

func (s *MySuite) TestOperation(c *C) {
	db,err := get_db("sqlite3")
        c.Assert(err, IsNil)
	db.init()
	operation := Operation{}
	operation.setDB(db)

	{
		log.Info("############ Executing SYNC Scenario ############")
		operation.init(func (db DB, cmd string)([]string, error){
			log.Info("In Execute")
			log.Info(cmd)
			return db.execute(cmd)
		})
		resp := operation.execute("TestCode1")
		resp.waitTillComplete()
		c.Assert(resp.getError(), IsNil)
		log.Info(resp)
		c.Assert(resp.getResult()[0], Equals, "Sqlite3-TestCode1" )

	}

        {
		log.Info("############ Executing SYNC TimedOut Scenario ############")
                operation.init(func (db DB, cmd string)([]string, error){
			time.Sleep(10*time.Second)
                        return db.execute(cmd)
                })
		operation.timeout = 3
                resp1 := operation.execute("TestCode2")
                resp1.waitTillComplete()
                c.Assert(resp1.getError().Error(), Equals, "TimedOut")
                log.Info(resp1)

        }
        time.Sleep(20*time.Second)
        {
                log.Info("############ Executing ASYNC TimedOut Scenario ############")
                operation.init(func (db DB, cmd string)([]string, error){
                        time.Sleep(10*time.Second)
                        return db.execute(cmd)
                })
                operation.timeout = 3
                resp1 := operation.execute("TestCode3")
                resp1.subscribe(func(cmd string, result []string){log.Fatal(cmd,"It Should not call result_func", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){if err.Error() != "TimedOut" {log.Fatal(cmd,"Invalid Error")}})
		time.Sleep(5*time.Second)
                c.Assert(resp1.getError().Error(), Equals, "TimedOut")
                log.Info(resp1)
        }
	time.Sleep(20*time.Second)
       {
                log.Info("############ Executing ASYNC Scenario ############")
                operation.init(func (db DB, cmd string)([]string, error){
                        return db.execute(cmd)
                })
                operation.timeout = 3
                resp := operation.execute("AsyncTask")
                resp.subscribe(func(cmd string, result []string){log.Info(cmd,":Result is ", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){log.Debug(cmd,": Error:",err)})
	}
	time.Sleep(20*time.Second)
        {
                log.Info("############ Executing ASYNC Expiry in QUEUE Scenario ############")
                operation.init(func (db DB, cmd string)([]string, error){
                        time.Sleep(25*time.Second)
                        return db.execute(cmd)
                })
                operation.timeout = 25
                resp := operation.execute("AsyncTask-4")
                resp.subscribe(func(cmd string, result []string){log.Info(cmd,":Result is ", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){log.Debug(cmd,": Error:",err)})
		operation.timeout = 20
                resp1 := operation.execute("TestCode-4")
		resp1.waitTillComplete()
		//resp1.subscribe(func(cmd string, result []string){log.Fatal(cmd,"It Should not call result_func", result)}, func(cmd string){log.Debug(cmd,":Task is Completed")}, func(cmd string, err error){if err.Error() != "Expired" {log.Fatal(cmd,": Invalid Error->", err.Error())}})
                //time.Sleep(30*time.Second)
                log.Info(resp)
                c.Assert(resp1.getError().Error(), Equals, "Expired")
                log.Info(resp1)
        }
}
